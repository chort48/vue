

const state = () =>({
    testValue:'startModule'
})
const getters = {
    getTestValue:s=>s.testValue
}

const mutations = {
    setTestValue(state, newVal){
        state.testValue = newVal
    }
}

const actions = {
    actionTest(store, param1, param2, param3){
        console.log(store)
    }
}

export default{
    state:state,
    getters:getters,
    mutations:mutations, 
    actions:actions
}