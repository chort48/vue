import Vue from 'vue'
import Vuex from 'vuex'
import  test from '@/store/modules/test.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    value:'startIndex'
  },
  getters: {
    value:s=>s.value
  },
  mutations: {
    value(state, val){
      state.value = val
    }
  },
  actions: {
    action(store, val){
      
    }
  },
  modules: {
    test
  }
})
